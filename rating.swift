//
//  rating.swift
//  politraker
//
//  Created by tbi-pc-57 on 12/1/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class rating: NSObject {
    
    class func Rating(with leg_id :String, user_id : String, rating : String, withSuccess success: @escaping ( _ dic: NSDictionary) -> Void, failure: @escaping ( _ err: String) -> Void) {
        
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=add_ratings")! )
        request.httpMethod = "POST"
        let postString = ("leg_id=\(leg_id)&user_id=\(user_id)&rating=\(rating)")
        request.httpBody = postString.data(using: .utf8)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request){ data, request, error in
            guard let data = data, error == nil else
            {
                print("error := \(String(describing: error))")
                let err = String (describing: error)
                
                failure(err )
                return;
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            // print(json!)
            success(json!)
            
        }
        task.resume()
        
    }

}
