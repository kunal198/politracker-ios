//
//  registerVote.swift
//  politraker
//
//  Created by tbi-pc-57 on 12/1/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class registerVote: NSObject {

    class func RegisterVote(with user_id :String, candidate_id : String, vote_yes : String,vote_no : String,vote_undecided : String, withSuccess success: @escaping ( _ dic: NSDictionary) -> Void, failure: @escaping ( _ err: String) -> Void) {
        
         print("User_id=\(user_id), Candidate_id=\(candidate_id), Vote_yes=\(vote_yes), Vote_no=\(vote_no), Votes_undecided=\(vote_undecided)")
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=register_vote")! )
        request.httpMethod = "POST"
        let postString = ("user_id=\(user_id)&candidate_id=\(candidate_id)&vote_yes=\(vote_yes)&vote_no=\(vote_no)&vote_undecided=\(vote_undecided)")
        request.httpBody = postString.data(using: .utf8)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request){ data, request, error in
            guard let data = data, error == nil else
            {
                print("error := \(String(describing: error))")
                let err = String (describing: error)
                
                failure(err )
                return;
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            // print(json!)
            success(json!)
            
        }
        task.resume()
        
    }
    
}
