//
//  election2018_issues.swift
//  politraker
//
//  Created by tbi-pc-57 on 11/28/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import AFNetworking

class election2018_issues: NSObject {
    
    class  func getting_issues( withSuccess success: @escaping ( _ dic: NSArray) -> Void, failure: @escaping ( _ err: String) -> Void)
        
    {
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        
        manager.get(
            "http://www.politrackervi.com/apis/api.php?api=issues",
            parameters: nil, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    let dataDict = jsonDict?.object(forKey: "data") as! NSArray
                    print("dataDict = \(String(describing: dataDict))")
                    
                    success(dataDict)
                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                failure(error as! String)
                print(error)
                print("Error: " + error.localizedDescription)
        })
    }

    class func Issues_Issue_id(with issue_id :String, withSuccess success: @escaping ( _ dic: NSDictionary) -> Void, failure: @escaping ( _ err: String) -> Void) {
        
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=issues_by_id")! )
        request.httpMethod = "POST"
        let postString = "issue_id=\(issue_id)"
        request.httpBody = postString.data(using: .utf8)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request){ data, request, error in
            guard let data = data, error == nil else
            {
                print("error := \(String(describing: error))")
                let err = String (describing: error)
                
                failure(err )
                return;
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            success(json!)
            
        }
        task.resume()
        
    }
    
}
