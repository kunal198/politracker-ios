//
//  ViewController_for_district.swift
//  politraker
//
//  Created by brst on 28/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import SDWebImage
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AFNetworking
//import TwitterCore
//import TwitterKit

class ViewController_for_district: UIViewController , UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIScrollViewDelegate{

    @IBOutlet weak var view_casteYourVote: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var BILL_SUMMARY: UIView!
    @IBOutlet weak var lbl_table_title: UILabel!
    @IBOutlet var view1: UIView!
    @IBOutlet weak var view_userSignIn: UIView!
    @IBOutlet weak var VIEW_RATING: UIView!
    @IBOutlet weak var view_userName: UIView!
    @IBOutlet weak var view_email: UIView!

    @IBOutlet var img_legislator: UIImageView!
    @IBOutlet var lbl_legislator_name: UILabel!
    @IBOutlet var lbl_party_name: UILabel!
    @IBOutlet var lbl_district_name: UILabel!
    @IBOutlet var lbl_votes: UILabel!
    @IBOutlet var star1: UIImageView!
    @IBOutlet var star2: UIImageView!
    @IBOutlet var star3: UIImageView!
    @IBOutlet var star4: UIImageView!
    @IBOutlet var star5: UIImageView!
    @IBOutlet var lbl_summary_title: UILabel!
    @IBOutlet var lbl_summary: UITextView!
    @IBOutlet var view_mainMenu_newUser: UIView!
    
    // Sign In
    @IBOutlet weak var txt_userSign_email: UITextField!
    @IBOutlet weak var txt_userSign_password: UITextField!
    
    
    
    // Menu - New User
    
    @IBOutlet var subView_newUser_userName: UIView!
    @IBOutlet var txt_newUser_userName: UITextField!
    @IBOutlet var subView_newUser_firstName: UIView!
    @IBOutlet var txt_newUser_firstName: UITextField!
    @IBOutlet var subView_newUser_lastName: UIView!
    @IBOutlet var txt_newUser_lastName: UITextField!
    @IBOutlet var subView_newUser_email: UIView!
    @IBOutlet var txt_newUser_email: UITextField!
    @IBOutlet var subView_newUser_password: UIView!
    @IBOutlet var txt_newUser_password: UITextField!
    
    // Rating Stars
    @IBOutlet weak var btn_ratingStar1: UIButton!
    @IBOutlet weak var btn_ratingStar2: UIButton!
    @IBOutlet weak var btn_ratingStar3: UIButton!
    @IBOutlet weak var btn_ratingStar4: UIButton!
    @IBOutlet weak var btn_ratingstar5: UIButton!
    var str_ratingStar = String()
    
    // Menu - Forget Password
    @IBOutlet var view_forgetPassword: UIView!
    @IBOutlet var subView_email: UIView!
    @IBOutlet var txt_forget_email: UITextField!
    
    @IBOutlet weak var lbl_noData: UILabel!
    // Menu - New User
    @IBOutlet weak var btn_twitter: UIButton!
    @IBOutlet weak var scr_signUp: UIScrollView!
    @IBOutlet weak var view_scrollView: UIView!
    @IBOutlet weak var btn_male: UIButton!
    @IBOutlet weak var btn_female: UIButton!
    @IBOutlet weak var subView_newUser_age: UIView!
    @IBOutlet weak var txt_age: UITextField!
    var str_gender = String()
    
    var data = NSMutableArray()
    var table_title = String()
    var checkCategory = String()
    var selected_legislator = String()
    var ratings = Int()
    var leg_id = String()
    
    var dict_bills = NSDictionary()
    var dict_category = NSDictionary()
    var array_bills = NSMutableArray()
    var array_billsSummary = NSMutableArray()
    var array_category = NSMutableArray()
    var array_categorySummary = NSMutableArray()
    var array_votes_yes = NSMutableArray()
    var array_votes_no = NSMutableArray()
    var array_votes_undecided = NSMutableArray()
    var array_votes_novote = NSMutableArray()
    
    var urlStr = NSMutableArray()
    var index1 = Int()
    var count = Int()
    var loginCheck = Bool()
    
    var array_category_SponsorCount = NSMutableArray()
    var array_CatId = NSMutableArray()
    var user_id = String()
    
    
    // FB Login
    var FBlogin  = FBSDKLoginManager()
    var fbAccessToken = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.scr_signUp.delegate = self
        self.scr_signUp.contentSize = CGSize(width: self.view_mainMenu_newUser.frame.size.width, height: self.view_scrollView.frame.size.height + 10)
        
        if UserDefaults.standard.object(forKey: "item_selected_legislator") != nil
        {
            self.selected_legislator = UserDefaults.standard.object(forKey: "item_selected_legislator") as! String
        }
        
        // sign up options
        self.str_gender = "male"
        self.btn_male.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_female.setImage(UIImage(named:"redb"), for: UIControlState.normal)
        
        self.viewOutllet()
        
        self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
        self.img_legislator.clipsToBounds = true
        
        data = ["H.R.1628: AMERICAN HEALTH CARE ACT OF 2017","S.1553: PAIN CAPABLE UNBORN CHILD PROTECTION ACT","H.R.2: MEDICARE ACCESS AND CHIP REAUTHRIZATION ACT","H.R.1628: AMERICAN HEALTH CARE ACT OF 2017","S.1553: PAIN CAPABLE UNBORN CHILD PROTECTION ACT","H.R.2: MEDICARE ACCESS AND CHIP REAUTHRIZATION ACT"]
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view1.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view1.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view1.addSubview(blurEffectView)
        }
       
        // Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(sender:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        view1.addGestureRecognizer(tapGesture)
    }
    override func viewWillAppear(_ animated: Bool)
    {
            lbl_table_title.text = table_title
            self.getting_legislatorDetails()
            self.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Gesture Tap Function:-
    func tapAction(sender:UITapGestureRecognizer)
    {
        self.view_mainMenu_newUser.isHidden = true
        self.view_forgetPassword.isHidden = true
        
        self.view_userSignIn.isHidden = true
        self.BILL_SUMMARY.isHidden = true
        self.VIEW_RATING.isHidden = true
        self.view1.isHidden = true
        
    }
    //MARK:<<< Table view Function
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if checkCategory == "category"
        {
            return self.array_category.count
        }
        else
        {
            return self.array_bills.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell_EDUCATION_WORKFORCE
        if checkCategory == "category"
        {
          
            
             let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: self.array_category[indexPath.row] as! String , attributes: underlineAttribute)

            cell.view_category.isHidden = false
            cell.lbl_title.attributedText = underlineAttributedString
            cell.lbl_votes_yes.text = self.array_votes_yes[indexPath.row] as? String
            cell.lbl_votes_no.text = self.array_votes_no[indexPath.row] as? String
            cell.lbl_votes_undecided.text = self.array_votes_undecided[indexPath.row] as? String
            cell.lbl_noVotes.text = self.array_votes_novote[indexPath.row] as? String
          //  let str = "SPONSOR: "
            let str2 = self.array_category_SponsorCount[indexPath.row] as! String//String(self.count)
            cell.lbl_sponsor_count.text = str2
            if cell.lbl_votes_yes.text == "0"         //self.votes_yes[indexPath.row] as? String == "0"
            {
                cell.img_yes.image = UIImage(named: "yes")
            }
            else
            {
                cell.img_yes.image = UIImage(named: "yes")
            }
            if cell.lbl_votes_no.text == "0"          //self.votes_no[indexPath.row] as? String == "0"
            {
                cell.img_no.image = UIImage(named: "cancel_votes")
            }
            else
            {
                cell.img_no.image = UIImage(named: "cancel_votes")
            }
            
        }
        else
        {
         
            cell.view_category.isHidden = true
            
             let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: self.array_bills[indexPath.row] as! String, attributes: underlineAttribute)

           cell.lbl_title.attributedText = underlineAttributedString
            
        }
       
    /*    let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
        let underlineAttributedString = NSAttributedString(string: data[indexPath.row] as! String, attributes: underlineAttribute)
        
        cell.lbl_title.attributedText = underlineAttributedString*/
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if self.checkCategory == "category"
        {
        
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "education&workforce") as! ViewController_edication_workforce
            nextViewController.cat_id = self.array_CatId[indexPath.row] as! String
            UserDefaults.standard.set(self.array_category[indexPath.row] as? String, forKey: "category_selected")
           // nextViewController.lbl_title_str.text = self.array_category[indexPath.row] as? String
            self.navigationController?.pushViewController(nextViewController, animated: true)

        }
        else
        {
            let data = self.array_billsSummary[indexPath.row] as? String
            //print(data!)
            if data! == "0"
            {
                self.view1.isHidden = false
                self.BILL_SUMMARY.isHidden = false
                self.index1 = indexPath.row
                
                self.lbl_summary_title.text = self.array_bills[indexPath.row] as? String//underlineAttributedString
                self.lbl_summary.text = ""
            }
            else
            {
                self.view1.isHidden = false
                self.BILL_SUMMARY.isHidden = false
                self.index1 = indexPath.row
            //    let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
         //       let underlineAttributedString = NSAttributedString(string: self.array_bills[indexPath.row] as! String, attributes: underlineAttribute)
                self.lbl_summary_title.text = self.array_bills[indexPath.row] as? String//underlineAttributedString
                self.lbl_summary.text = self.array_billsSummary[indexPath.row] as? String
            }
        }
    }
    @IBAction func btn_bills(_ sender: Any)
    {
        self.getting_Bills()
        lbl_table_title.text = "BILLS & VOTES"
        self.checkCategory = "bills"
        self.tableView.reloadData()
    }
    @IBAction func btn_category(_ sender: Any)
    {
        self.getting_Category()
        lbl_table_title.text = "CATEGORIES & VOTES"
        self.checkCategory = "category"
        self.tableView.reloadData()
    }
    @IBAction func btn_goback(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func casteYourVote(_ sender: Any)
    {
        if UserDefaults.standard.object(forKey: "loginCheck") != nil
        {
            let loginCheck = UserDefaults.standard.object(forKey: "loginCheck") as? Bool
            if loginCheck == true
            {
                self.VIEW_RATING.isHidden = false
                self.view_userSignIn.isHidden = true
                self.view1.isHidden = false
            }
            else
            {
                self.view_userSignIn.isHidden = false
                self.view1.isHidden = false
            }
        }
        else
        {
            self.view_userSignIn.isHidden = false
            self.view1.isHidden = false
        }
    }
    @IBAction func userSignIn_exit(_ sender: Any)
    {
        self.view_userSignIn.isHidden = true
        self.view1.isHidden = true
        
    }
    @IBAction func btn_userSignIn(_ sender: Any)
    {
        self.loginData()
    }
    @IBAction func btn_Star1(_ sender: Any)
    {
        self.str_ratingStar = "1"
        self.btn_ratingStar1.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        
        self.btn_ratingStar2.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        self.btn_ratingStar3.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        self.btn_ratingStar4.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
       
        self.btn_ratingstar5.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
    
    }
    @IBAction func btn_Star2(_ sender: Any)
    {
        self.str_ratingStar = "2"
        self.btn_ratingStar1.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar2.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        
        self.btn_ratingStar3.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        self.btn_ratingStar4.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        self.btn_ratingstar5.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
    }
    @IBAction func btn_Star3(_ sender: Any)
    {
        self.str_ratingStar = "3"
        self.btn_ratingStar1.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar2.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar3.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        
        self.btn_ratingStar4.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        self.btn_ratingstar5.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
    }
    @IBAction func btn_Star4(_ sender: Any)
    {
        self.str_ratingStar = "4"
        self.btn_ratingStar1.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar2.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar3.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar4.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        
        self.btn_ratingstar5.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
    }
    
    @IBAction func btn_Star5(_ sender: Any)
    {
        self.str_ratingStar = "5"
        self.btn_ratingStar1.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar2.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar3.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingStar4.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
        self.btn_ratingstar5.setImage(UIImage(named: "star_rating"), for: UIControlState.normal)
    }
    
    
    @IBAction func btn_rating_submit(_ sender: Any)
    {
        if self.str_ratingStar.isEmpty
        {
            let alert = UIAlertController(title: "Alert", message: "Rate from 1 - 5", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if UserDefaults.standard.object(forKey: "user_id") != nil
            {
                self.user_id = UserDefaults.standard.object(forKey: "user_id") as! String
            }
            
            print("leg_id=\(self.leg_id), user_id=\(self.user_id), rating =\(self.str_ratingStar)")
            rating.Rating(with: self.leg_id,user_id: self.user_id,rating: self.str_ratingStar, withSuccess:{ (result) in
                let json = result
                
                print(json)
                if json.object(forKey: "status") as? String == "400"
                {
                    print(json.object(forKey: "description") as! String)
                }
                else
                {
                    DispatchQueue.main.async {
                        
                        let alertController = UIAlertController(title: "Rated Successfully.", message: nil, preferredStyle: UIAlertControllerStyle.alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                            (result : UIAlertAction) -> Void in
                            self.view1.isHidden = true
                            self.VIEW_RATING.isHidden = true
                            self.btn_ratingStar1.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
                            
                            self.btn_ratingStar2.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
                            self.btn_ratingStar3.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
                            self.btn_ratingStar4.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
                            
                            self.btn_ratingstar5.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
                            print("OK")
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            },
                          failure: { (error) in
                            print (error)
            });
            
        }

    }
    
    @IBAction func btn_rating_exit(_ sender: Any)
    {
        self.VIEW_RATING.isHidden = true
        self.view1.isHidden = true
        self.btn_ratingStar1.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        
        self.btn_ratingStar2.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        self.btn_ratingStar3.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        self.btn_ratingStar4.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
        
        self.btn_ratingstar5.setImage(UIImage(named: "rating_star"), for: UIControlState.normal)
    }
    @IBAction func btn_billSummary_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.BILL_SUMMARY.isHidden = true
    }
    
    func getting_Bills()
    {
        self.array_bills.removeAllObjects()
        self.array_billsSummary.removeAllObjects()
        self.urlStr.removeAllObjects()
        
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=legislators_bill")!)
        request.httpMethod = "POST"
        
        let postString = "leg_id=\(leg_id)"
        request.httpBody = postString.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary //Dictionary<String, AnyObject>
            if((json) != nil){
                print(json!)
                if json? .object(forKey: "status") as? String == "400"
                {
                    print(json?.object(forKey: "description") as! String)
                    DispatchQueue.main.async {
                        self.lbl_noData.isHidden = false
                    }
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.lbl_noData.isHidden = true
                    }
                    
                let data = json?.value(forKey: "data") as!NSArray
              // print(data)
                
                if  data.value(forKey: "values") is [NSArray]
                {
                    let resultarr = data.value(forKey: "values") as! NSArray
                    let resultarr2 = resultarr[0] as! NSArray
                    //print(resultarr2)
                    for i in resultarr2
                    {
                        let indexvalue = i as! NSDictionary
                        self.array_bills.add(indexvalue.value(forKey: "name_leb") as! String)
                        
                        
                        if  let val = indexvalue["summary_leb"] as? String {
                            self.array_billsSummary.add(indexvalue.value(forKey: "summary_leb") as! String)
                            
                        } else
                        {
                            self.array_billsSummary.add("0")
                            

                        }
                        if let urlVal = indexvalue["url"] as? String {
                            self.urlStr.add(indexvalue.value(forKey: "url") as! String)
                        }
                        else
                        {
                            self.urlStr.add(0)
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }                        

                    }
                    
                   // print(self.urlStr)
                }
                else
                {
                    //
                }
                }
            }
        }
        task.resume()

    }
    
    func getting_Category()
    {
        self.array_category.removeAllObjects()
        self.array_votes_yes.removeAllObjects()
        self.array_votes_no.removeAllObjects()
        self.array_CatId.removeAllObjects()
        self.array_category_SponsorCount.removeAllObjects()
        self.array_categorySummary.removeAllObjects()
        
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=legislators_cat")!)
        request.httpMethod = "POST"
        
        let postString = "leg_id=\(leg_id)"
        request.httpBody = postString.data(using: .utf8)
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            if((json) != nil){
            print(json!)
            if json? .object(forKey: "status") as? String == "400"
            {
                print(json?.object(forKey: "description") as! String)
                DispatchQueue.main.async {
                    self.lbl_noData.isHidden = false
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.lbl_noData.isHidden = true
                }
                
                let data = json?.value(forKey: "data") as!NSArray
               print(data)
//                if  data.value(forKey: "values") is [NSArray]
//                {
//                    let resultarr = data.value(forKey: "values") as! NSArray
//                    let resultarr2 = resultarr[0] as! NSArray
//                    let count1 = data.value(forKey: "count") as! NSArray
//                  //  print(count1)
//                    self.count = count1[0] as! Int
                
              
                    for i in data
                    {
                        let indexvalue = i as! NSDictionary
                        print(indexvalue)
                        self.array_category.add(indexvalue.value(forKey: "category_name") as! String)
                        self.array_votes_yes.add(indexvalue.value(forKey: "totalyea_leb") as! String)
                        self.array_votes_no.add(indexvalue.value(forKey: "totalnay_leb") as! String)
                        self.array_votes_undecided.add(indexvalue.value(forKey: "vote_absent") as! String)
                        self.array_votes_novote.add(indexvalue.value(forKey: "vote_novoting") as! String)
                        self.array_CatId.add(indexvalue.value(forKey: "category_id") as! String)
                        self.array_category_SponsorCount.add(indexvalue.value(forKey: "sponsors_count") as! String)
                        if  indexvalue.value(forKey: "bill_summary") is [String]
                        {
                            self.array_categorySummary.add(indexvalue.value(forKey: "bill_summary") as! String)
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                    }
               // }
//                else
//                {
//                    //
//                }
            }
        }
        task.resume()

    }
    
    func getting_legislatorDetails()
    {
        // Getting  api data for Legislator
        legislatorData .gettingData(withSuccess: { (result) in
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.selected_legislator == dict.value(forKey: "name_leg") as! String
                {
                    let str111 = dict.value(forKey: "name_leg") as? String
                    self.lbl_legislator_name.text = str111?.uppercased()            //dict.value(forKey: "name_leg") as? String
                    let str112 = dict.value(forKey: "party_leg") as? String
                    self.lbl_party_name.text = str112?.uppercased()                     //dict.value(forKey: "party_leg") as? String
                    let str113 = dict.value(forKey: "name_dis") as? String
                    self.lbl_district_name.text = str113?.uppercased()          //dict.value(forKey: "name_dis") as? String
                    
                    self.leg_id = dict.value(forKey: "id_leg") as! String
                    self.ratings = (dict.value(forKey: "ratings") as? Int)!
                    
                    switch self.ratings
                    {
                    case 1:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "rating_star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                    case 2:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                    case 3:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        break
                    case 4:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "star")
                        self.star5.image = UIImage(named: "rating_star")
                        break
                    case 5:
                        self.star1.image = UIImage(named: "star")
                        self.star2.image = UIImage(named: "star")
                        self.star3.image = UIImage(named: "star")
                        self.star4.image = UIImage(named: "star")
                        self.star5.image = UIImage(named: "star")
                        break
                    default:
                        self.star1.image = UIImage(named: "rating_star")
                        self.star2.image = UIImage(named: "rating_star")
                        self.star3.image = UIImage(named: "rating_star")
                        self.star4.image = UIImage(named: "rating_star")
                        self.star5.image = UIImage(named: "rating_star")
                        
                        break
                        
                    }
                    let votes = dict.value(forKey: "votes") as! Int
                    self.lbl_votes.text = ("\("(")\(votes)\(")")")
                    
                    let img_url = dict.value(forKey: "img_url") as? String
                    
                    self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                    
                    self.getting_Bills()
                    self.getting_Category()
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }

                }
                
            }
            
           // print(dictData)
        }, failure: { (error) in
            print (error)
        });

    }
    
    
    func viewOutllet()
    {
        
        // view board style :
        self.view_casteYourVote.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_casteYourVote.layer.shadowOpacity = 0.5
        self.view_casteYourVote.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_casteYourVote.layer.shadowRadius = 10
        //self.view_casteYourVote.layer.cornerRadius = 20
        self.view_casteYourVote.layer.masksToBounds = false
        self.view_casteYourVote.layer.shadowPath = UIBezierPath(rect: self.view_casteYourVote.bounds).cgPath
        self.view_casteYourVote.layer.shouldRasterize = true
        self.view_casteYourVote.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_userName.layer.borderWidth = 0.6
        self.view_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.view_email.layer.borderWidth = 0.6
        self.view_email.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_userSignIn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_userSignIn.layer.shadowOpacity = 0.5
        self.view_userSignIn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_userSignIn.layer.shadowRadius = 10
        self.view_userSignIn.layer.cornerRadius = 20
        self.view_userSignIn.layer.masksToBounds = false
        self.view_userSignIn.layer.shadowPath = UIBezierPath(rect: self.view_userSignIn.bounds).cgPath
        self.view_userSignIn.layer.shouldRasterize = true
        self.view_userSignIn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.VIEW_RATING.layer.shadowColor = UIColor.lightGray.cgColor
        self.VIEW_RATING.layer.shadowOpacity = 0.5
        self.VIEW_RATING.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.VIEW_RATING.layer.shadowRadius = 10
        self.VIEW_RATING.layer.cornerRadius = 20
        self.VIEW_RATING.layer.masksToBounds = false
        self.VIEW_RATING.layer.shadowPath = UIBezierPath(rect: self.VIEW_RATING.bounds).cgPath
        self.VIEW_RATING.layer.shouldRasterize = true
        self.VIEW_RATING.layer.rasterizationScale =  UIScreen.main.scale
        
        self.BILL_SUMMARY.layer.shadowColor = UIColor.lightGray.cgColor
        self.BILL_SUMMARY.layer.shadowOpacity = 0.5
        self.BILL_SUMMARY.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.BILL_SUMMARY.layer.shadowRadius = 10
        self.BILL_SUMMARY.layer.cornerRadius = 20
        self.BILL_SUMMARY.layer.masksToBounds = false
        self.BILL_SUMMARY.layer.shadowPath = UIBezierPath(rect: self.BILL_SUMMARY.bounds).cgPath
        self.BILL_SUMMARY.layer.shouldRasterize = true
        self.BILL_SUMMARY.layer.rasterizationScale =  UIScreen.main.scale

        // view board style :
        self.view_mainMenu_newUser.layer.shadowColor = UIColor.lightGray.cgColor
        self.subView_newUser_userName.layer.borderWidth = 0.6
        self.subView_newUser_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_firstName.layer.borderWidth = 0.6
        self.subView_newUser_firstName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_lastName.layer.borderWidth = 0.6
        self.subView_newUser_lastName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_email.layer.borderWidth = 0.6
        self.subView_newUser_email.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_password.layer.borderWidth = 0.6
        self.subView_newUser_password.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_age.layer.borderWidth = 0.6
        self.subView_newUser_age.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_mainMenu_newUser.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_mainMenu_newUser.layer.shadowOpacity = 0.5
        self.view_mainMenu_newUser.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_mainMenu_newUser.layer.shadowRadius = 10
        self.view_mainMenu_newUser.layer.cornerRadius = 20
        self.view_mainMenu_newUser.layer.masksToBounds = false
        self.view_mainMenu_newUser.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_newUser.bounds).cgPath
        self.view_mainMenu_newUser.layer.shouldRasterize = true
        self.view_mainMenu_newUser.layer.rasterizationScale =  UIScreen.main.scale
        self.view_scrollView.layer.cornerRadius = 40
        self.view_scrollView.layer.masksToBounds = false
        
        
        
        
        self.subView_email.layer.borderWidth = 0.6
        self.subView_email.layer.borderColor = UIColor.lightGray.cgColor
        self.view_forgetPassword.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_forgetPassword.layer.shadowOpacity = 0.5
        self.view_forgetPassword.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_forgetPassword.layer.shadowRadius = 10
        self.view_forgetPassword.layer.cornerRadius = 20
        self.view_forgetPassword.layer.masksToBounds = false
        self.view_forgetPassword.layer.shadowPath = UIBezierPath(rect: self.view_forgetPassword.bounds).cgPath
        self.view_forgetPassword.layer.shouldRasterize = true
        self.view_forgetPassword.layer.rasterizationScale =  UIScreen.main.scale
    }
    
    //MARK: Bill Summary Title _ PDF
    @IBAction func btn_billSummary_title_pdf(_ sender: Any)
    {
        if self.urlStr.count >= 0
        {
            let str = self.urlStr[index1] as! String
            let url = URL(string: str)  //URL(string: "http://www.google.com")!
            if #available(iOS 10.0, *)
            {
                UIApplication.shared.open(url! , options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url! )
            }
        }
    }
    
    //MARK: <<<<<<<<<<  >>>>>>> Sign Up here !!
    //MARK: <<< Main Menu <New User Sign Up>
    @IBAction func btn_mainMenu_newUser_signUP(_ sender: Any)
    {
        if self.txt_newUser_userName.text != "" && self.txt_newUser_firstName.text != "" && self.txt_newUser_lastName.text != "" && self.txt_newUser_email.text != "" && self.txt_newUser_password.text != ""
        {
            self.PostData()
            self.txt_newUser_userName.text = ""
            self.txt_newUser_firstName.text = ""
            self.txt_newUser_lastName.text = ""
            self.txt_newUser_email.text = ""
            self.txt_newUser_password.text = ""
        }
        else
        {
            if self.txt_newUser_userName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER USER NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
            }
            if self.txt_newUser_firstName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER FIRST NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            if self.txt_newUser_lastName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER LAST NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            if self.txt_newUser_email.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER EMAIL ID",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            if self.txt_newUser_password.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER PASSWORD",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func btn_maleSelected(_ sender: Any)
    {
        self.str_gender = "male"
        self.btn_male.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_female.setImage(UIImage(named:"redb"), for: UIControlState.normal)
    }
    
    @IBAction func btn_femaleSelected(_ sender: Any)
    {
        self.str_gender = "female"
        self.btn_female.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_male.setImage(UIImage(named:"redb"), for: UIControlState.normal)
    }
    //MARK: Sign Up Post Data -
    func PostData(){
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["username": self.txt_newUser_userName.text!, "first_name" : self.txt_newUser_firstName.text!, "lastname": self.txt_newUser_lastName.text!, "email": txt_newUser_email.text!, "password": self.txt_newUser_password.text!, "age": self.txt_age.text!, "gender": self.str_gender]
        
        manager.post("http://www.politrackervi.com/apis/signup.php", parameters: parameters, progress: nil, success: {(_ operation: URLSessionDataTask, _ responseObject: Any) -> Void in
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                
                let description: String = jsonDict?.value(forKey: "description") as! String
                let status : String = jsonDict?.value(forKey: "status") as! String
                if status == "400"
                {
                    let alert = UIAlertController(
                        title: "ALERT",
                        message: description,
                        preferredStyle: UIAlertControllerStyle.alert)
                    
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                }
                else if status == "200"
                {
                    self.view_mainMenu_newUser.isHidden = true
                    //self.view_mainMenu_NewUser_thanks.isHidden = false
                    
                    self.txt_newUser_userName.text = ""
                    self.txt_newUser_firstName.text = ""
                    self.txt_newUser_lastName.text = ""
                    self.txt_newUser_email.text = ""
                    self.txt_newUser_password.text = ""
                    self.txt_age.text = ""
                    self.view_mainMenu_newUser.isHidden = true
                    // self.view_mainMenu_NewUser_thanks.isHidden = false
                    // Thanks screen
                    
                }
                else if status == "201"
                {
                    let alert = UIAlertController(
                        title: "ALERT",
                        message: description,
                        preferredStyle: UIAlertControllerStyle.alert)
                    
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            catch{
                print("throws exception")
            }
            
        }, failure: {( task : URLSessionDataTask!, error: Error!) -> Void in
            
            let alert = UIAlertController(
                title: "Error",
                message: "CAN'T SIGN UP, WITH THESE USER DETAILS",
                preferredStyle: UIAlertControllerStyle.alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            }
            
            alert.addAction(OKAction)
            
            self.present(alert, animated: true, completion: nil)
            
            print("Error: \(error)")
        })
    }
    
    //MARK:<<< Menu - New User Sign Up : Facebook Login
    @IBAction func btn_facebook_signUp(_ sender: Any)
    {
        let alert = UIAlertController(title: "in progess", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
       /* FBlogin = FBSDKLoginManager()
        
        self.FBlogin .logIn(withReadPermissions:["public_profile","user_friends","user_photos","user_location","user_education_history","user_birthday","user_posts"], from: self) { (result, error) in
            if (error == nil)
            {
                if(result?.isCancelled)!
                {
                    
                } else {
                    
                    OperationQueue.main.addOperation {
                        [weak self] in
                        
                        self?.fbAccessToken = FBSDKAccessToken.current().tokenString
                        self?.getFaceBookDetails()
                    }
                    
                    OperationQueue.main.addOperation
                        {
                            [weak self] in
                            
                    }
                }
            }
            else
            {
                print(error!)
                //     print(error?.localizedDescription)
                OperationQueue.main.addOperation {
                    [weak self] in
                    // Util.showMessage("There is some Network Problem!", withTitle: "Alert!")
                }
            }
            
        }*/
    }
    func getFaceBookDetails()
    {
        if ((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,birthday,gender,hometown,location,about"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    print(result as Any)
                    print(error as Any)
                    //let id = String()
                    var fname = String()
                    var lname = String()
                    var url = String()
                    var userName = String()
                    var user_id = String()
                    let fbDetails = result as! NSDictionary
                    fname = fbDetails.value(forKey: "first_name") as! String
                    lname = fbDetails.value(forKey: "last_name") as! String
                    user_id = fbDetails.value(forKey: "id") as! String
                    UserDefaults.standard.set(user_id, forKey: "user_id")
                    if fname == "" && lname != ""
                    {
                        userName = ("logged as : \(lname)")
                    }
                    if lname == "" && fname != ""
                    {
                        userName = ("logged as : \(fname)")
                    }
                    else
                    {
                        userName = ("logged as : \(fname + lname)")
                    }
                    UserDefaults.standard.set(userName, forKey: "UserName")
                    self.loginCheck = true
                    UserDefaults.standard.set(self.loginCheck, forKey: "loginCheck")
                    self.view1.isUserInteractionEnabled = true
                    self.view1.isHidden = true
                    self.view_mainMenu_newUser.isHidden = true
                    // self.view_main_menu.isHidden = true
                    // self.login.setTitle("USER SIGN OUT", for: UIControlState.normal)
                    
                    let alert = UIAlertController(title: nil, message: userName, preferredStyle: .alert)
                    self.present(alert, animated: true, completion: nil)
                    
                    // change to desired number of seconds (in this case 5 seconds)
                    let when = DispatchTime.now() + 1
                    DispatchQueue.main.asyncAfter(deadline: when){
                        // your code with delay
                        alert.dismiss(animated: true, completion: nil)
                    }
                    
                    //  let dict1 = fbDetails.value(forKey: "picture") as! NSDictionary
                    //  let dict2 = dict1.value(forKey: "data") as! NSDictionary
                    //  url = dict2.value(forKey: "url") as! String
                    //  let id = FBSDKAccessToken.current().userID
                    //  print("\(id)\(fname)\(lname)\(url)")
                } else {
                    print(result!)
                }
            })
        }
    }
    
    
    
    //MARK: <<< Menu - New User Sign Up: Google Plus Login
    @IBAction func btn_google_signUp(_ sender: Any) {
        let alert = UIAlertController(title: "in progess", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:<<<  Menu - New User Sign Up: Twitter Login
    @IBAction func btn_twitter_signUp(_ sender: Any) {
        let alert = UIAlertController(title: "in progess", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        /*
         Twitter.sharedInstance().logIn(completion: { (session, error) in
         if (session != nil) {
         print("signed in as \(String(describing: session?.userName))");
         let client = TWTRAPIClient.withCurrentUser()
         
         client.requestEmail { email, error in
         if (email != nil) {
         print("signed in as \(String(describing: session?.userName))");
         } else {
         print("error: \(String(describing: error?.localizedDescription))");
         }
         }
         } else {
         print("error: \(String(describing: error?.localizedDescription))");
         }
         })
         
         */
    }
    @IBAction func btn_userSignIn_SignUp(_ sender: Any)
    {
        self.view_userSignIn.isHidden = true
        self.view_mainMenu_newUser.isHidden = false
    }
    @IBAction func btn_newUser_forgetPassword_btn(_ sender: Any)
    {
        self.view_userSignIn.isHidden = true
        self.view_forgetPassword.isHidden = false
    }
    @IBAction func btn_forget_submit(_ sender: Any)
    {
        self.txt_forget_email.text = ""
        self.forgetData()
    }
    @IBAction func btn_forget_exit(_ sender: Any)
    {
        self.view_forgetPassword.isHidden = true
        self.txt_forget_email.text = ""
        self.view1.isHidden = true
    }
    @IBAction func btn_mainMenu_newUser_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view_mainMenu_newUser.isHidden = true
        self.view1.isUserInteractionEnabled = true
        self.txt_newUser_userName.text = ""
        self.txt_newUser_firstName.text = ""
        self.txt_newUser_lastName.text = ""
        self.txt_newUser_email.text = ""
        self.txt_newUser_password.text = ""
        self.txt_age.text = ""
    }
    
    func loginData() {
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["username": self.txt_userSign_email.text!, "password" : self.txt_userSign_password.text!]
        
        manager.post(
            "http://www.politrackervi.com/apis/login.php",
            parameters: parameters, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    print(jsonDict)
                    let description: String = jsonDict?.value(forKey: "description") as! String
                    let status : String = jsonDict?.value(forKey: "status") as! String
                    if status == "400"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if status == "200"
                    {
                        let dict = jsonDict?.value(forKey: "data") as! NSDictionary
                        let user_id = dict.value(forKey: "id_usr") as! String
                        // setting User_id here !
                        UserDefaults.standard.set(user_id, forKey: "user_id")
                        self.VIEW_RATING.isHidden = false
                        self.view_userSignIn.isHidden = true
                        self.view1.isHidden = false
                        
                        self.loginCheck = true
                        UserDefaults.standard.set(self.loginCheck, forKey: "loginCheck")
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                            
                        }
                        alert.addAction(OKAction)
                        
                        
                        self.present(alert, animated: true, completion: nil)
                        self.txt_userSign_email.text = ""
                        self.txt_userSign_password.text = ""
                        
                    }
                    
                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                
                print(error)
                let alert = UIAlertController(
                    title: "Error",
                    message: "USER DOESN'T EXIST",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                print("Error: " + error.localizedDescription)
        })
    }
    
    //MARK: Forget Password -
    func forgetData()
    {
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["email": self.txt_forget_email.text!]
        
        manager.post(
            "http://www.politrackervi.com/apis/forgot_pasword.php",
            parameters: parameters, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    let description: String = jsonDict?.value(forKey: "description") as! String
                    let status : String = jsonDict?.value(forKey: "status") as! String
                    if status == "400"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if status == "200"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                
                print(error)
                let alert = UIAlertController(
                    title: "Error",
                    message: "USER DOESN'T EXIST",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                print("Error: " + error.localizedDescription)
        })
        
        
    }
    
}
