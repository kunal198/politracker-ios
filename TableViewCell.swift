//
//  TableViewCell.swift
//  politraker
//
//  Created by Brst on 6/24/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet var img: UIImageView!
    @IBOutlet var lbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
