//
//  ViewController_election2018_2.swift
//  politraker
//
//  Created by brst on 28/06/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import SDWebImage
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import AFNetworking
//import TwitterCore
//import TwitterKit

class ViewController_election2018_2: UIViewController, UITableViewDelegate, UITableViewDataSource, GIDSignInUIDelegate, UIScrollViewDelegate {

    @IBOutlet weak var btn_menu_title: UIButton!
    @IBOutlet weak var lbl_menu_title: UILabel!
    @IBOutlet weak var view_menu_btn: UIView! //
    @IBOutlet weak var view_aboutUs: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_johnDoe: UIView!
    @IBOutlet weak var view_searchByCandidate: UIView!
    @IBOutlet weak var tableView1: UITableView! //
    @IBOutlet var lbl_educationOrCandidate: UILabel!
    
    @IBOutlet weak var lbl_noData: UILabel!
    @IBOutlet var img_legislator: UIImageView!
    @IBOutlet var lbl_legislator_name: UILabel!
    @IBOutlet var lbl_party_name: UILabel!
    @IBOutlet var lbl_district_name: UILabel!
    @IBOutlet var lbl_legislator_list: UILabel!
    
    @IBOutlet var btn_undecided: UIButton!
    @IBOutlet var btn_no: UIButton!
    @IBOutlet var btn_yes: UIButton!
    @IBOutlet var view_mainMenu_UserSignIn: UIView!
    @IBOutlet var view_mainMenu_newUser: UIView!
    // Menu - New User
    
    @IBOutlet var subView_newUser_userName: UIView!
    @IBOutlet var txt_newUser_userName: UITextField!
    @IBOutlet var subView_newUser_firstName: UIView!
    @IBOutlet var txt_newUser_firstName: UITextField!
    @IBOutlet var subView_newUser_lastName: UIView!
    @IBOutlet var txt_newUser_lastName: UITextField!
    @IBOutlet var subView_newUser_email: UIView!
    @IBOutlet var txt_newUser_email: UITextField!
    @IBOutlet var subView_newUser_password: UIView!
    @IBOutlet var txt_newUser_password: UITextField!
    
    // Menu - User Sign in
    @IBOutlet var subView_mainmenu_userSignIn_userName: UIView!
    @IBOutlet var subView_mainMenu_userSignIn_email: UIView!
    @IBOutlet var txt_userSign_email: UITextField!
    @IBOutlet var txt_userSign_password: UITextField!
    
    // Menu - Forget Password
    @IBOutlet var view_forgetPassword: UIView!
    @IBOutlet var subView_email: UIView!
    @IBOutlet var txt_forget_email: UITextField!
    
    // Menu - New User
    @IBOutlet weak var btn_twitter: UIButton!
    @IBOutlet weak var scr_signUp: UIScrollView!
    @IBOutlet weak var view_scrollView: UIView!
    @IBOutlet weak var btn_male: UIButton!
    @IBOutlet weak var btn_female: UIButton!
    @IBOutlet weak var subView_newUser_age: UIView!
    @IBOutlet weak var txt_age: UITextField!
    var str_gender = String()
    
    
    var selected_legislator = String()
    var checkStr = String()
    var didSelect = Bool()
    var legislatorArray = NSMutableArray()
    var partyArray = NSMutableArray()
    var districtArray = NSMutableArray()
    var imgArray = NSMutableArray()

    var legislatorArray1 = NSMutableArray()
    var billsArray1 = NSMutableArray()
    var districtArray1 = NSMutableArray()
    var categoryArray1 = NSMutableArray()
    
    var selected_legislator1 = String()
    var selected_bills1 = String()
    var selected_district1 = String()
    var selected_category1 = String()
    
    var issue_id = String()
    var candidacy_id = String()
    
    var bill_idArray1 = NSMutableArray()
    var cat_idArray1 = NSMutableArray()
    var district_idArray1 = NSMutableArray()
    var strTitle = String()
    
    var user_id = String()
    var candidate_id = String()
    var str_vote_yes = String()
    var str_vote_no = String()
    var str_vote_undecided = String()
    var candidate_idArray = NSMutableArray()
    
    var loginCheck = Bool()
    
    // FB Login
    var FBlogin  = FBSDKLoginManager()
    var fbAccessToken = String()
    
        override func viewDidLoad() {
        super.viewDidLoad()
        self.viewOutlet()
        self.view2.isHidden = true
        self.scr_signUp.delegate = self
        self.scr_signUp.contentSize = CGSize(width: self.view_mainMenu_newUser.frame.size.width, height: self.view_scrollView.frame.size.height + 10)
        
        self.getting_LegislatorList()
        self.getting_BillsList()
        self.getting_DistrictList()
        self.getting_CategoryList()
        
        // sign up options
        self.str_gender = "male"
        self.btn_male.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_female.setImage(UIImage(named:"redb"), for: UIControlState.normal)
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view1.backgroundColor = UIColor.white
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view1.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view1.addSubview(blurEffectView)
        }
        
      
    }
    override func viewWillAppear(_ animated: Bool)
    {
            if checkStr == "issues"
            {
                self.lbl_educationOrCandidate.text = strTitle//"EDUCATION & WORKFORCE ISSUES"
                self.view_searchByCandidate.isHidden = true
                self.IssuesByIssue_id()
            }
            if checkStr == "candidates"
            {
                self.lbl_educationOrCandidate.text = "SEARCH BY CANDIDATES"
                self.view_searchByCandidate.isHidden = false
                if UserDefaults.standard.object(forKey: "item_selected_legislator") != nil
                {
                    self.selected_legislator = UserDefaults.standard.object(forKey: "item_selected_legislator") as! String
                }
                self.getting_election2018Candidates()
                //self.getting_legislatorDetails()
            }
            if checkStr == "candidacy"
            {
                self.lbl_educationOrCandidate.text = strTitle//"CANDIDATE & RUNEMATE"
                self.view_searchByCandidate.isHidden = true
                self.CandidacyByCandidacy_id()
            }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: Table view Function
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView1
        {
            if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
            {
                return self.legislatorArray1.count
            }
            if self.lbl_menu_title.text == "CHOOSE THE BILLS"
            {
                return self.billsArray1.count
            }
            if self.lbl_menu_title.text == "CHOOSE CATEGORY"
            {
                return self.categoryArray1.count
            }
            if self.lbl_menu_title.text == "CHOOSE DISTRICT"
            {
                return self.districtArray1.count
            }
            
        }
        return self.legislatorArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableView1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell

            if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
            {
                cell.lbl.text = self.legislatorArray1[indexPath.row] as? String
                return cell
                
            }
            if self.lbl_menu_title.text == "CHOOSE THE BILLS"
            {
                cell.lbl.text = self.billsArray1[indexPath.row] as? String
                return cell
                
            }
            if self.lbl_menu_title.text == "CHOOSE CATEGORY"
            {
                cell.lbl.text = self.categoryArray1[indexPath.row] as? String
                return cell
                
            }
            if self.lbl_menu_title.text == "CHOOSE DISTRICT"
            {
                cell.lbl.text = self.districtArray1[indexPath.row] as? String
                return cell
                
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell_GoBACK_2
        
        let str = self.legislatorArray[indexPath.row] as? String
        cell.lbl_legislator_name.text = str?.uppercased()
        let str1 = self.partyArray[indexPath.row] as? String
        cell.lbl_party_name.text = str1?.uppercased()
        let str2 = self.districtArray[indexPath.row] as? String
        cell.lbl_district_name.text = str2?.uppercased()
        
        guard let img_url = self.imgArray[indexPath.row] as? String
            else
        {
            return cell
        }
       // if img_url != "0"
       // {
        cell.img_legislator.sd_setImage(with: URL(string: img_url), placeholderImage: UIImage(named: "avtar"))
            cell.img_legislator.layer.cornerRadius = cell.img_legislator.frame.size.width/2
            cell.img_legislator.clipsToBounds = true
            cell.img_legislator.frame.size.height = cell.img_legislator.frame.size.width
       // }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableView1
        {
            if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
            {
                self.lbl_legislator_list.text = self.legislatorArray[indexPath.row] as? String
                self.selected_legislator1 = (self.legislatorArray[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
            if self.lbl_menu_title.text == "CHOOSE THE BILLS"
            {
                self.lbl_legislator_list.text = self.billsArray1[indexPath.row] as? String
                self.selected_bills1 = (self.billsArray1[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
            if self.lbl_menu_title.text == "CHOOSE DISTRICT"
            {
                self.lbl_legislator_list.text = self.districtArray1[indexPath.row] as? String
                self.selected_district1 = (self.districtArray1[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
            if self.lbl_menu_title.text == "CHOOSE CATEGORY"
            {
                self.lbl_legislator_list.text = self.categoryArray1[indexPath.row] as? String
                self.selected_category1 = (self.categoryArray1[indexPath.row] as? String)!
                self.tableView1.isHidden = true
            }
           
        }
        else if tableView == tableView
        {
            self.didSelect = true
            self.lbl_educationOrCandidate.text = "SEARCH BY CANDIDATES"
            self.candidate_id = self.candidate_idArray[indexPath.row] as!String
            self.lbl_legislator_name.text = self.legislatorArray[indexPath.row] as? String
            self.lbl_district_name.text = self.districtArray[indexPath.row] as? String
            self.lbl_party_name.text = self.partyArray[indexPath.row] as? String
            let imgUrl = self.imgArray[indexPath.row]
            
            self.img_legislator.sd_setImage(with: URL(string: imgUrl as! String), placeholderImage: UIImage(named: "avtar"))
            self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
            self.img_legislator.clipsToBounds = true
            self.view_searchByCandidate.isHidden = false
        }
    }
    @IBAction func btn_education(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view_aboutUs.isHidden = false
    }
    @IBAction func btn_aboutUs_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view_aboutUs.isHidden = true
    }
    @IBAction func btn_menu_submit(_ sender: Any)
    {
        if self.lbl_menu_title.text == "CHOOSE LEGISLATOR"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2") as! ViewController2
            nextViewController.selected_legislator = self.selected_legislator1   //  $$$$$
            nextViewController.checkStr = "legislator"
            UserDefaults.standard.removeObject(forKey: "viewController2_subview")
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_legislator")

            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //
        if self.lbl_menu_title.text == "CHOOSE THE BILLS"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_bills")

            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.strBtnValue = "chooseBills"
            nextViewController.selected_bills = self.selected_bills1
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if self.lbl_menu_title.text == "CHOOSE DISTRICT"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_district")

            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
            nextViewController.selected_district = self.selected_district1
            nextViewController.strBtnValue = "chooseDistrict"
            nextViewController.strCategory = true
            nextViewController.strDistrict = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        if self.lbl_menu_title.text == "CHOOSE CATEGORY"
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            UserDefaults.standard.set(self.lbl_legislator_list.text, forKey: "item_selected_category")

            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "view2.1_subView") as! ViewController2_SUBVIEW_GO_BACK_
           // nextViewController.selected_category = self.selected_category1
            nextViewController.strBtnValue = "chooseCategory"
            nextViewController.strCategory = true
            nextViewController.strDistrict = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
       }
    @IBAction func btn_category(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE CATEGORY"
        self.lbl_legislator_list.text = self.categoryArray1[0] as? String
       // self.selected_category1 = (self.categoryArray1[0] as? String)!
       // self.btn_menu_title .setTitle(   self.categoryArray1[0] as? String , for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_bills(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE THE BILLS"
        self.lbl_legislator_list.text = self.billsArray1[0] as? String
       // self.selected_bills1 = (self.billsArray1[0] as? String)!

       // self.btn_menu_title .setTitle(self.billsArray1[0] as? String , for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_district(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE DISTRICT"
        self.lbl_legislator_list.text = self.districtArray1[0] as? String
       // self.selected_legislator1 = (self.districtArray1[0] as? String)!

       // self.btn_menu_title .setTitle(   self.districtArray1[0] as? String , for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_legislator(_ sender: Any)
    {
        self.view1.isHidden = false
        self.view2.isHidden = false
        self.view_menu_btn.isHidden = false
        self.lbl_menu_title.text = "CHOOSE LEGISLATOR"
        self.lbl_legislator_list.text = self.legislatorArray1[0] as? String
      //  self.selected_legislator1 = (self.legislatorArray1[0] as? String)!

       // self.btn_menu_title .setTitle(   self.legislatorArray[0] as? String, for: UIControlState.normal)
        self.tableView1.reloadData()
    }
    @IBAction func btn_menu_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view2.isHidden = true
        self.view_menu_btn.isHidden = true
    }
    @IBAction func btn_menu_title_selected(_ sender: Any)
    {
        if self.tableView1.isHidden == false
        {
            self.tableView1.isHidden = true
        }
        else
        {
            self.tableView1.isHidden = false
        }
       // self.tableView1.isHidden = false
    }
    @IBAction func btn_goBack(_ sender: Any)
    {
        if didSelect == true
        {
            didSelect = false
            if checkStr == "issues"
            {
                self.lbl_educationOrCandidate.text = "EDUCATION & WORKFORCE ISSUES"
                self.view_searchByCandidate.isHidden = true
            }
            if checkStr == "candidacy"
            {
                self.lbl_educationOrCandidate.text = "CANDIDATE & RUNEMATE"
                self.view_searchByCandidate.isHidden = true
            }
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func homeAction(_ sender: Any)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK: votes YES
    @IBAction func btn_yes_clicked(_ sender: Any)
    {
        self.str_vote_yes = "1"
        self.str_vote_no = "0"
        self.str_vote_undecided = "0"
        self.btn_yes.setImage(UIImage(named:"green_s_copy_20"), for: UIControlState.normal)
        self.btn_no.setImage(UIImage(named:"redb"), for: UIControlState.normal)
        self.btn_undecided.setImage(UIImage(named: "black_copy_20"), for: UIControlState.normal)
    }
    //MARK: votes NO
    @IBAction func btn_no_clicked(_ sender: Any)
    {
        self.str_vote_yes = "0"
        self.str_vote_no = "1"
        self.str_vote_undecided = "0"
        self.btn_no.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_yes.setImage(UIImage(named:"green_copy_20"), for: UIControlState.normal)
        self.btn_undecided.setImage(UIImage(named: "black_copy_20"), for: UIControlState.normal)
    }
    //MARK: votes UNDECIDED
    @IBAction func btn_undecided_clicked(_ sender: Any)
    {
        self.str_vote_yes = "0"
        self.str_vote_no = "0"
        self.str_vote_undecided = "1"
        self.btn_undecided.setImage(UIImage(named:"black_s_copy_20"), for: UIControlState.normal)
        self.btn_no.setImage(UIImage(named:"redb"), for: UIControlState.normal)
        self.btn_yes.setImage(UIImage(named: "green_copy_20"), for: UIControlState.normal)
    }
  
    //MARK: <<<<<<<<<<  >>>>>>> Sign Up here !!
    //MARK: <<< Main Menu <New User Sign Up>
    @IBAction func btn_mainMenu_newUser_signUP(_ sender: Any)
    {
        if self.txt_newUser_userName.text != "" && self.txt_newUser_firstName.text != "" && self.txt_newUser_lastName.text != "" && self.txt_newUser_email.text != "" && self.txt_newUser_password.text != ""
        {
            self.PostData()
            self.txt_newUser_userName.text = ""
            self.txt_newUser_firstName.text = ""
            self.txt_newUser_lastName.text = ""
            self.txt_newUser_email.text = ""
            self.txt_newUser_password.text = ""
        }
        else
        {
            if self.txt_newUser_userName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER USER NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
            }
            if self.txt_newUser_firstName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER FIRST NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            if self.txt_newUser_lastName.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER LAST NAME",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            if self.txt_newUser_email.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER EMAIL ID",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            if self.txt_newUser_password.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER PASSWORD",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func btn_maleSelected(_ sender: Any)
    {
        self.str_gender = "male"
        self.btn_male.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_female.setImage(UIImage(named:"redb"), for: UIControlState.normal)
    }
    
    @IBAction func btn_femaleSelected(_ sender: Any)
    {
        self.str_gender = "female"
        self.btn_female.setImage(UIImage(named:"red-s_copy_20"), for: UIControlState.normal)
        self.btn_male.setImage(UIImage(named:"redb"), for: UIControlState.normal)
    }
    //MARK: Sign Up Post Data -
    func PostData(){
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["username": self.txt_newUser_userName.text!, "first_name" : self.txt_newUser_firstName.text!, "lastname": self.txt_newUser_lastName.text!, "email": txt_newUser_email.text!, "password": self.txt_newUser_password.text!, "age": self.txt_age.text!, "gender": self.str_gender]
        
        
        
        manager.post("http://www.politrackervi.com/apis/signup.php", parameters: parameters, progress: nil, success: {(_ operation: URLSessionDataTask, _ responseObject: Any) -> Void in
            do {
                let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                
                let description: String = jsonDict?.value(forKey: "description") as! String
                let status : String = jsonDict?.value(forKey: "status") as! String
                if status == "400"
                {
                    let alert = UIAlertController(
                        title: "ALERT",
                        message: description,
                        preferredStyle: UIAlertControllerStyle.alert)
                    
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                }
                else if status == "200"
                {
                    self.view_mainMenu_newUser.isHidden = true
                    //self.view_mainMenu_NewUser_thanks.isHidden = false
                    
                    self.txt_newUser_userName.text = ""
                    self.txt_newUser_firstName.text = ""
                    self.txt_newUser_lastName.text = ""
                    self.txt_newUser_email.text = ""
                    self.txt_newUser_password.text = ""
                    self.txt_age.text = ""
                    self.view_mainMenu_newUser.isHidden = true
                    // self.view_mainMenu_NewUser_thanks.isHidden = false
                    // Thanks screen
                    
                }
                else if status == "201"
                {
                    let alert = UIAlertController(
                        title: "ALERT",
                        message: description,
                        preferredStyle: UIAlertControllerStyle.alert)
                    
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                }
                
                
            }
            catch{
                print("throws exception")
            }
            
        }, failure: {( task : URLSessionDataTask!, error: Error!) -> Void in
            
            let alert = UIAlertController(
                title: "Error",
                message: "CAN'T SIGN UP, WITH THESE USER DETAILS",
                preferredStyle: UIAlertControllerStyle.alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            }
            
            alert.addAction(OKAction)
            
            self.present(alert, animated: true, completion: nil)
            
            print("Error: \(error)")
        })
    }
    
    //MARK:<<< Menu - New User Sign Up : Facebook Login
    @IBAction func btn_facebook_signUp(_ sender: Any)
    {
        FBlogin = FBSDKLoginManager()
        
        self.FBlogin .logIn(withReadPermissions:["public_profile","user_friends","user_photos","user_location","user_education_history","user_birthday","user_posts"], from: self) { (result, error) in
            if (error == nil)
            {
                if(result?.isCancelled)!
                {
                    
                } else {
                    
                    OperationQueue.main.addOperation {
                        [weak self] in
                        
                        self?.fbAccessToken = FBSDKAccessToken.current().tokenString
                        self?.getFaceBookDetails()
                    }
                    
                    OperationQueue.main.addOperation
                        {
                            [weak self] in
                            
                    }
                }
            }
            else
            {
                print(error!)
                //     print(error?.localizedDescription)
                OperationQueue.main.addOperation {
                    [weak self] in
                    // Util.showMessage("There is some Network Problem!", withTitle: "Alert!")
                }
            }
            
        }
    }
    func getFaceBookDetails()
    {
        if ((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email,birthday,gender,hometown,location,about"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    print(result as Any)
                    print(error as Any)
                    //let id = String()
                    var fname = String()
                    var lname = String()
                    var url = String()
                    var userName = String()
                    var user_id = String()
                    let fbDetails = result as! NSDictionary
                    fname = fbDetails.value(forKey: "first_name") as! String
                    lname = fbDetails.value(forKey: "last_name") as! String
                    user_id = fbDetails.value(forKey: "id") as! String
                    UserDefaults.standard.set(user_id, forKey: "user_id")
                    if fname == "" && lname != ""
                    {
                        userName = ("logged as : \(lname)")
                    }
                    if lname == "" && fname != ""
                    {
                        userName = ("logged as : \(fname)")
                    }
                    else
                    {
                        userName = ("logged as : \(fname + lname)")
                    }
                    UserDefaults.standard.set(userName, forKey: "UserName")
                    self.loginCheck = true
                    UserDefaults.standard.set(self.loginCheck, forKey: "loginCheck")
                    self.view1.isUserInteractionEnabled = true
                    self.view1.isHidden = true
                    self.view_mainMenu_newUser.isHidden = true
                    // self.view_main_menu.isHidden = true
                    // self.login.setTitle("USER SIGN OUT", for: UIControlState.normal)
                    
                    let alert = UIAlertController(title: nil, message: userName, preferredStyle: .alert)
                    self.present(alert, animated: true, completion: nil)
                    
                    // change to desired number of seconds (in this case 5 seconds)
                    let when = DispatchTime.now() + 1
                    DispatchQueue.main.asyncAfter(deadline: when){
                        // your code with delay
                        alert.dismiss(animated: true, completion: nil)
                    }
                    
                    //  let dict1 = fbDetails.value(forKey: "picture") as! NSDictionary
                    //  let dict2 = dict1.value(forKey: "data") as! NSDictionary
                    //  url = dict2.value(forKey: "url") as! String
                    //  let id = FBSDKAccessToken.current().userID
                    //  print("\(id)\(fname)\(lname)\(url)")
                } else {
                    print(result!)
                }
            })
        }
    }
    
    
    
    //MARK: <<< Menu - New User Sign Up: Google Plus Login
    @IBAction func btn_google_signUp(_ sender: Any) {
    }
    
    //MARK:<<<  Menu - New User Sign Up: Twitter Login
    @IBAction func btn_twitter_signUp(_ sender: Any) {
        /*
         Twitter.sharedInstance().logIn(completion: { (session, error) in
         if (session != nil) {
         print("signed in as \(String(describing: session?.userName))");
         let client = TWTRAPIClient.withCurrentUser()
         
         client.requestEmail { email, error in
         if (email != nil) {
         print("signed in as \(String(describing: session?.userName))");
         } else {
         print("error: \(String(describing: error?.localizedDescription))");
         }
         }
         } else {
         print("error: \(String(describing: error?.localizedDescription))");
         }
         })
         
         */
    }
    @IBAction func btn_userSignIn_SignUp(_ sender: Any)
    {
        self.view_mainMenu_UserSignIn.isHidden = true
        self.view_mainMenu_newUser.isHidden = false
    }
    @IBAction func btn_newUser_forgetPassword_btn(_ sender: Any)
    {
        self.view_mainMenu_UserSignIn.isHidden = true
        self.view_forgetPassword.isHidden = false
    }
    @IBAction func btn_forget_submit(_ sender: Any)
    {
        self.txt_forget_email.text = ""
        self.forgetData()
    }
    @IBAction func btn_forget_exit(_ sender: Any)
    {
        self.view_forgetPassword.isHidden = true
        self.txt_forget_email.text = ""
        self.view1.isHidden = true
    }
    @IBAction func btn_mainMenu_newUser_exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view_mainMenu_newUser.isHidden = true
        self.view1.isUserInteractionEnabled = true
        self.txt_newUser_userName.text = ""
        self.txt_newUser_firstName.text = ""
        self.txt_newUser_lastName.text = ""
        self.txt_newUser_email.text = ""
        self.txt_newUser_password.text = ""
        self.txt_age.text = ""
    }
    
    @IBAction func btn_mainMenu_UserSignIn_Exit(_ sender: Any)
    {
        self.view1.isHidden = true
        self.view_mainMenu_UserSignIn.isHidden = true
        self.view1.isUserInteractionEnabled = true
        self.txt_userSign_email.text = ""
        self.txt_userSign_password.text = ""
    }
    @IBAction func btn_mainMenu_SignIn(_ sender: Any)
    {
        if self.txt_userSign_email.text != "" && self.txt_userSign_password.text != ""
        {
            self.loginData()
        }
        else
        {
            if self.txt_userSign_email.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER EMAIL ID",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
            if self.txt_userSign_password.text == ""
            {
                let alert = UIAlertController(
                    title: "Error",
                    message: "PLEASE ENTER PASSWORD",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                
            }
        }
    }
    @IBAction func casteYourVote(_ sender: Any)
    {
        if self.str_vote_yes != ""
        {
            if UserDefaults.standard.object(forKey: "loginCheck") != nil
            {
                let loginCheck = UserDefaults.standard.object(forKey: "loginCheck") as? Bool
                if loginCheck == true
                {
                    self.RegisterForVote()
                }
                else
                {
                    self.view_mainMenu_UserSignIn.isHidden = false
                    self.view1.isHidden = false
                }
            }
            else
            {
                self.view_mainMenu_UserSignIn.isHidden = false
                self.view1.isHidden = false
            }
        }
        else
        {
            let alert = UIAlertController(title: "Alert", message: "Please Select the Any Options", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
   
    
    //MARK: Sign In login Data -
    func loginData() {
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["username": self.txt_userSign_email.text!, "password" : self.txt_userSign_password.text!]
        
        manager.post(
            "http://www.politrackervi.com/apis/login.php",
            parameters: parameters, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    let description: String = jsonDict?.value(forKey: "description") as! String
                    let status : String = jsonDict?.value(forKey: "status") as! String
                    if status == "400"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if status == "200"
                    {
                        let dict = jsonDict?.value(forKey: "data") as! NSDictionary
                        let user_id = dict.value(forKey: "id_usr") as! String
                        // setting User_id here !
                        UserDefaults.standard.set(user_id, forKey: "user_id")
                        
                        
                        self.view1.isUserInteractionEnabled = true
                        self.view_mainMenu_UserSignIn.isHidden = true
                       // self.view_main_menu.isHidden = true
                        self.view1.isHidden = true
                        self.loginCheck = true
                     //   self.login.setTitle("USER SIGN OUT", for: UIControlState.normal)
                        UserDefaults.standard.set(self.loginCheck, forKey: "loginCheck")
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
//                            if self.election2018_login_check == true
//                            {
//                                self.election2018_login_check = false
//                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//
//                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "election2018") as! ViewController_election2018_2
//                                nextViewController.checkStr = "candidates"
//                                self.navigationController?.pushViewController(nextViewController, animated: true)
//                            }
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                        self.txt_userSign_email.text = ""
                        self.txt_userSign_password.text = ""
                        
                    }
                    
                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                
                print(error)
                let alert = UIAlertController(
                    title: "Error",
                    message: "USER DOESN'T EXIST",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                print("Error: " + error.localizedDescription)
        })
    }
    //MARK: Forget Password -
    func forgetData()
    {
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()
        let parameters: [String: String] = ["email": self.txt_forget_email.text!]
        
        manager.post(
            "http://www.politrackervi.com/apis/forgot_pasword.php",
            parameters: parameters, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    
                    let description: String = jsonDict?.value(forKey: "description") as! String
                    let status : String = jsonDict?.value(forKey: "status") as! String
                    if status == "400"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if status == "200"
                    {
                        let alert = UIAlertController(
                            title: "ALERT",
                            message: description,
                            preferredStyle: UIAlertControllerStyle.alert)
                        
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                
                print(error)
                let alert = UIAlertController(
                    title: "Error",
                    message: "USER DOESN'T EXIST",
                    preferredStyle: UIAlertControllerStyle.alert)
                
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
                }
                
                alert.addAction(OKAction)
                
                self.present(alert, animated: true, completion: nil)
                print("Error: " + error.localizedDescription)
        })
        
        
    }
    func getting_legislatorDetails()
    {
        // Getting  api data for Legislator
        legislatorData .gettingData(withSuccess: { (result) in
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.selected_legislator == dict.value(forKey: "name_leg") as! String
                {
                    let str111 = dict.value(forKey: "name_leg") as? String
                    self.lbl_legislator_name.text = str111?.uppercased()            //dict.value(forKey: "name_leg") as? String
                    let str112 = dict.value(forKey: "party_leg") as? String
                    self.lbl_party_name.text = str112?.uppercased()                     //dict.value(forKey: "party_leg") as? String
                    let str113 = dict.value(forKey: "name_dis") as? String
                    self.lbl_district_name.text = str113?.uppercased()          //dict.value(forKey: "name_dis") as? String
                    
                //    self.leg_id = dict.value(forKey: "id_leg") as! String
                    
                   
                    
                    let img_url = dict.value(forKey: "img_url") as? String
                    
                    self.img_legislator.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "avtar"))
                    self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
                    self.img_legislator.clipsToBounds = true
                }
                
            }
            
        }, failure: { (error) in
            print (error)
        });
        
    }

    //MARK: <<<****************>>  Issues using issue_id  
    
    func IssuesByIssue_id()
    {
       self.legislatorArray.removeAllObjects()
        self.imgArray.removeAllObjects()
        self.partyArray.removeAllObjects()
        // Getting  api data for Bill_id
        election2018_issues.Issues_Issue_id(with: self.issue_id, withSuccess:{ (result) in
            let json = result
            // if(json != nil){
            print(json)
            if json.object(forKey: "status") as? String == "400"
            {
                print(json.object(forKey: "description") as! String)
                DispatchQueue.main.async {
                    self.lbl_noData.isHidden = false
                }
            }
            else
            {
                let dictData = json.object(forKey: "data") as! NSArray
                
                // }
                // let dictData = dataDict as NSDictionary
                print(dictData)
                for index in 0..<dictData.count
                {
                    var dict = NSDictionary()
                    dict = dictData[index] as! NSDictionary
                    // print(dict)
                    
                    if self.legislatorArray.contains(dict.value(forKey: "candidate_name") as! String)
                    {   }
                    else
                    {
                        self.legislatorArray.addObjects(from: [dict.value(forKey: "candidate_name")! ])
                        self.partyArray.addObjects(from: [dict.value(forKey: "party_name")! ])
                        self.imgArray.add(dict.value(forKey: "thumbnail")! )
                        self.districtArray.add(dict.value(forKey: "district_name") as! String)
                        self.candidate_idArray.add(dict.value(forKey: "candidate_id") as! String)
                    }
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        },
         failure: { (error) in
            print (error)
        });
    }
    //MARK: <<<****************>>  Candidcay using candidacy_id
    
    func CandidacyByCandidacy_id()
    {
        self.legislatorArray.removeAllObjects()
        self.imgArray.removeAllObjects()
        self.partyArray.removeAllObjects()
        
        election2018_candidacy.Candidacy_Candidacy_id(with: self.candidacy_id, withSuccess:{ (result) in
            let json = result
            print(json)
            if json .object(forKey: "status") as? String == "400"
            {
                print(json.object(forKey: "description") as! String)
                DispatchQueue.main.async {
                    self.lbl_noData.isHidden = false
                }
            }
            else
            {
                let dictData = json.object(forKey: "data") as! NSArray
                
                print(dictData)
                for index in 0..<dictData.count
                {
                    var dict = NSDictionary()
                    dict = dictData[index] as! NSDictionary
                    // print(dict)
                    
                    if self.legislatorArray.contains(dict.value(forKey: "candidate_name") as! String)
                    {   }
                    else
                    {
                        self.legislatorArray.addObjects(from: [dict.value(forKey: "candidate_name")! ])
                        self.partyArray.addObjects(from: [dict.value(forKey: "party_name")! ])
                        self.districtArray.addObjects(from: [dict.value(forKey: "district_name")! ])
                        self.imgArray.add(dict.value(forKey: "thumbnail")! )
                        self.candidate_idArray.add(dict.value(forKey: "candidate_id") as! String)
                    }
                    
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        },
                                            failure: { (error) in
                                                print (error)
        });
    }
    
    //MARK:  Register for Vote
    func RegisterForVote()
    {
       if UserDefaults.standard.object(forKey: "user_id") != nil
       {
            self.user_id = UserDefaults.standard.object(forKey: "user_id") as! String
        }
        print("User_id=\(self.user_id), Candidate_id=\(self.candidate_id), Vote_yes=\(self.str_vote_yes), Vote_no=\(self.str_vote_no), Votes_undecided=\(self.str_vote_undecided)")
        registerVote.RegisterVote(with: self.user_id, candidate_id: self.candidate_id, vote_yes: self.str_vote_yes, vote_no: self.str_vote_no, vote_undecided: self.str_vote_undecided,  withSuccess:{ (result) in
            let json = result
            print(json)
            let dictData = json.object(forKey: "data") as! NSDictionary
            let msg = dictData.value(forKey: "message") as! String
            print(msg)
            if json .object(forKey: "status") as? String == "400"
            {
                print(json.object(forKey: "description") as! String)
               
                let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        },
          failure: { (error) in
            print (error)
        });
    }
    
    
    func getting_LegislatorList()
    {
        // Getting  api data for Legislator
        legislatorData .gettingData(withSuccess: { (result) in
            //   print (result)
            let dictData = result as NSMutableArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.legislatorArray1.contains(dict.value(forKey: "name_leg") as! String)
                {
                    
                }
                else
                {
                    self.legislatorArray1.add(dict.value(forKey: "name_leg") as! String)
                }
            }
            //  self.arrayElectionList3 = self.legislatorArray.mutableCopy() as! NSMutableArray
            
        }, failure: { (error) in
            print (error)
        });
    }
    
    func getting_BillsList()
    {
        // Getting  api data for Bills
     
        billsData .getting_Bills_Data(withSuccess: { (result) in
            print (result)
            let dictData = result as NSArray
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                print(dict)
                if self.billsArray1.contains(dict.value(forKey: "bill_name") as! String)
                {   }
                else
                {
                    self.billsArray1.add(dict.value(forKey: "bill_name") as! String)
                    self.bill_idArray1.add(dict.value(forKey: "bill_id") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        
    }
    
    func getting_DistrictList()
    {
        // Getting  api data for District
        districtData .getting_District_Data(withSuccess: { (result) in
            //  print (result)
            let dictData = result as NSMutableArray
            // print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                
                if self.districtArray1.contains(dict.value(forKey: "district_name") as! String)
                {   }
                else
                {
                    self.districtArray1.add(dict.value(forKey: "district_name") as! String)
                    self.district_idArray1.add(dict.value(forKey: "id") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
    }
    
    func getting_CategoryList()
    {
        
        // Getting  api data for Category
        categoryData .getting_Category_Data(withSuccess: { (result) in
            //   print (result)
            let dictData = result as NSMutableArray
            //  print(dictData)
            for index in 0..<dictData.count
            {
                var dict = NSDictionary()
                dict = dictData[index] as! NSDictionary
                if self.categoryArray1.contains(dict.value(forKey: "cat_name") as! String)
                {   }
                else
                {
                    self.categoryArray1.add(dict.value(forKey: "cat_name") as! String)
                    self.cat_idArray1.add(dict.value(forKey: "id") as! String)
                }
            }
            
        }, failure: { (error) in
            print (error)
        });
        
    }
    func getting_election2018Candidates()
    {
        election2018_candidates.getting_candidates(withSuccess: { (result) in
            print(result)
            let demoArr = result as NSMutableArray
            
            for index in 0..<demoArr.count
            {
                var dict = NSDictionary()
                dict = demoArr[index] as! NSDictionary
                if self.candidate_id == dict.value(forKey: "candidate_id") as! String
                {
                    let str111 = dict.value(forKey: "candidate_name") as? String
                    self.lbl_legislator_name.text = str111?.uppercased()
                    let str112 = dict.value(forKey: "party_name") as? String
                    self.lbl_party_name.text = str112?.uppercased()
                    let str113 = dict.value(forKey: "district_name") as? String
                    self.lbl_district_name.text = str113?.uppercased()
                    
                    
                    guard let img_url = dict.value(forKey: "thumbnail") as? String else { return }
                    self.img_legislator.sd_setImage(with: URL(string: img_url), placeholderImage: UIImage(named: "avtar"))
                    self.img_legislator.layer.cornerRadius = self.img_legislator.frame.size.width/2
                    self.img_legislator.clipsToBounds = true
     
                }
               
                
            }
            
        }
            , failure: { (error) in
                print(error)
        });
    }
    
    func viewOutlet()
    {
        
        
        self.view_mainMenu_newUser.layer.shadowColor = UIColor.lightGray.cgColor
       
        
        // view board style :
        self.subView_newUser_userName.layer.borderWidth = 0.6
        self.subView_newUser_userName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_firstName.layer.borderWidth = 0.6
        self.subView_newUser_firstName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_lastName.layer.borderWidth = 0.6
        self.subView_newUser_lastName.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_email.layer.borderWidth = 0.6
        self.subView_newUser_email.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_password.layer.borderWidth = 0.6
        self.subView_newUser_password.layer.borderColor = UIColor.lightGray.cgColor
        self.subView_newUser_age.layer.borderWidth = 0.6
        self.subView_newUser_age.layer.borderColor = UIColor.lightGray.cgColor
        
        self.view_mainMenu_newUser.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_mainMenu_newUser.layer.shadowOpacity = 0.5
        self.view_mainMenu_newUser.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_mainMenu_newUser.layer.shadowRadius = 10
        self.view_mainMenu_newUser.layer.cornerRadius = 20
        self.view_mainMenu_newUser.layer.masksToBounds = false
        self.view_mainMenu_newUser.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_newUser.bounds).cgPath
        self.view_mainMenu_newUser.layer.shouldRasterize = true
        self.view_mainMenu_newUser.layer.rasterizationScale =  UIScreen.main.scale
        self.view_scrollView.layer.cornerRadius = 40
        self.view_scrollView.layer.masksToBounds = false
        
        //        // view board style :
                self.subView_mainmenu_userSignIn_userName.layer.borderWidth = 0.6
                self.subView_mainmenu_userSignIn_userName.layer.borderColor = UIColor.lightGray.cgColor
                self.subView_mainMenu_userSignIn_email.layer.borderWidth = 0.6
                self.subView_mainMenu_userSignIn_email.layer.borderColor = UIColor.lightGray.cgColor
        
                self.view_mainMenu_UserSignIn.layer.shadowColor = UIColor.lightGray.cgColor
                self.view_mainMenu_UserSignIn.layer.shadowOpacity = 0.5
                self.view_mainMenu_UserSignIn.layer.shadowOffset = CGSize(width: -1, height: 1)
                self.view_mainMenu_UserSignIn.layer.shadowRadius = 10
                self.view_mainMenu_UserSignIn.layer.cornerRadius = 20
                self.view_mainMenu_UserSignIn.layer.masksToBounds = false
                self.view_mainMenu_UserSignIn.layer.shadowPath = UIBezierPath(rect: self.view_mainMenu_UserSignIn.bounds).cgPath
                self.view_mainMenu_UserSignIn.layer.shouldRasterize = true
                self.view_mainMenu_UserSignIn.layer.rasterizationScale =  UIScreen.main.scale
        
        self.subView_email.layer.borderWidth = 0.6
        self.subView_email.layer.borderColor = UIColor.lightGray.cgColor
        self.view_forgetPassword.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_forgetPassword.layer.shadowOpacity = 0.5
        self.view_forgetPassword.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_forgetPassword.layer.shadowRadius = 10
        self.view_forgetPassword.layer.cornerRadius = 20
        self.view_forgetPassword.layer.masksToBounds = false
        self.view_forgetPassword.layer.shadowPath = UIBezierPath(rect: self.view_forgetPassword.bounds).cgPath
        self.view_forgetPassword.layer.shouldRasterize = true
        self.view_forgetPassword.layer.rasterizationScale =  UIScreen.main.scale
        
        self.btn_menu_title.layer.cornerRadius = 5
        self.btn_menu_title.layer.borderWidth = 1
        self.btn_menu_title.layer.borderColor = UIColor.gray.cgColor
        
        // view board style :
        self.view_johnDoe.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_johnDoe.layer.shadowOpacity = 0.5
        self.view_johnDoe.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_johnDoe.layer.shadowRadius = 10
        self.view_johnDoe.layer.cornerRadius = 20
        self.view_johnDoe.layer.masksToBounds = false
        self.view_johnDoe.layer.shadowPath = UIBezierPath(rect: self.view_johnDoe.bounds).cgPath
        self.view_johnDoe.layer.shouldRasterize = true
        self.view_johnDoe.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_aboutUs.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_aboutUs.layer.shadowOpacity = 0.5
        self.view_aboutUs.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_aboutUs.layer.shadowRadius = 10
        self.view_aboutUs.layer.cornerRadius = 20
        self.view_aboutUs.layer.masksToBounds = false
        self.view_aboutUs.layer.shadowPath = UIBezierPath(rect: self.view_johnDoe.bounds).cgPath
        self.view_aboutUs.layer.shouldRasterize = true
        self.view_aboutUs.layer.rasterizationScale =  UIScreen.main.scale
        
        self.view_menu_btn.layer.shadowColor = UIColor.lightGray.cgColor
        self.view_menu_btn.layer.shadowOpacity = 0.5
        self.view_menu_btn.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.view_menu_btn.layer.shadowRadius = 10
        self.view_menu_btn.layer.cornerRadius = 20
        self.view_menu_btn.layer.masksToBounds = false
        self.view2.layer.masksToBounds = false  // view2
        self.view_menu_btn.layer.shadowPath = UIBezierPath(rect: self.view_menu_btn.bounds).cgPath
        self.view_menu_btn.layer.shouldRasterize = true
        self.view_menu_btn.layer.rasterizationScale =  UIScreen.main.scale
        
    }
}
