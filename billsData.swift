//
//  billsData.swift
//  politraker
//
//  Created by Brst on 7/12/17.
//  Copyright © 2017 Brihaspati. All rights reserved.
//

import UIKit
import AFNetworking

class billsData: NSObject {
    
    class  func getting_Bills_Data( withSuccess success: @escaping ( _ dic: NSArray) -> Void, failure: @escaping ( _ err: String) -> Void)
    {
      
     /*   let request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=bills_info")!)

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            guard let data = data, error == nil else {
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
            if((json) != nil){
                
                let dataDict = json?.object(forKey: "data") as! NSArray

                print(dataDict)
                success(dataDict)
    
            }
        }
        task.resume()
        
     */
      
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFHTTPResponseSerializer()

        manager.get(
            "http://www.politrackervi.com/apis/api.php?api=bills_info",
            parameters: nil, progress: nil,
            success:
            {
                (operation, responseObject) -> Void in
                
                do {
                    
                    
                    let json = try JSONSerialization.jsonObject(with: responseObject as! Data, options: []) as? NSDictionary
                    
                    print(json!)
                    
                    let jsonDict = try JSONSerialization.jsonObject(with: responseObject as! Data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                    print(jsonDict!)
                    let dataDict = jsonDict?.object(forKey: "data") as! NSMutableArray
                    print("dataDict = \(String(describing: dataDict))")
                    
                    success(dataDict)
                                }catch{
                    
                }
                
        },
            failure:
            {
                (operation, error) in
                
                print(error)
                print("Error: " + error.localizedDescription)
                failure(error as! String)
        })
  //  }


}
    class func BillsUsing_bill_id(with bill_id :String, withSuccess success: @escaping ( _ dic: NSDictionary) -> Void, failure: @escaping ( _ err: String) -> Void) {
        
        var request = URLRequest(url: URL(string: "http://www.politrackervi.com/apis/api.php?api=bills_info_by_id")! )
        request.httpMethod = "POST"
        //print(bill_id)
        let postString = "bill_id=\(bill_id)"
        request.httpBody = postString.data(using: .utf8)
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("0", forHTTPHeaderField: "Content-Length")

        let task = URLSession.shared.dataTask(with: request){ data, request, error in
            guard let data = data, error == nil else
            {
                print("error := \(String(describing: error))")
                let err = String (describing: error)
                
                failure(err )
                return;
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
          //  print(json)
            success(json!)
            
        }
        task.resume()
        
    }
    
}
